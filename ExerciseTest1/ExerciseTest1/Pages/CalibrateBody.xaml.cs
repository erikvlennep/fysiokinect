﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using ExerciseTest1.Bones;
using ExerciseTest1.Camera;
using ExerciseTest1.Joints;
using Microsoft.Kinect;

namespace ExerciseTest1.Pages
{
    /// <summary>
    /// Interaction logic for CalibrateBody.xaml
    /// </summary>
    public partial class CalibrateBody : Window
    {
        /// <summary>
        /// Needed for activating and deactivating the KinectSensor
        /// </summary>
        private KinectSensor _sensor;

        /// <summary>
        /// Multiple frames are needed
        /// </summary>
        private MultiSourceFrameReader _frameReader;

        /// <summary>
        /// Check all the bodies in the room. max: 6
        /// </summary>
        private Body[] bodies;

        /// <summary>
        /// The colorcamera of the kinect. 
        /// </summary>
        private ColorCamera colorCamera;

        /// <summary>
        /// The width of the canvas
        /// </summary>
        public double canvasWidth;

        /// <summary>
        /// The height of the canvas
        /// </summary>
        public double canvasHeight;

        /// <summary>
        /// Connection between jointtypes and joints
        /// </summary>
        private Dictionary<JointType, MyJoint> jointsDictionary;

        /// <summary>
        /// The bones for the connection between joints 
        /// </summary>
        private List<MyBone> bonesDictionary;

        /// <summary>
        /// Timer for calibration
        /// </summary>
        private int _timer;

        /// <summary>
        /// Check calibration once
        /// </summary>
        private bool _calibrated;

        /// <summary>
        /// Keep track of the first person to join the room
        /// </summary>
        private ulong _oneBody;

        public CalibrateBody()
        {
            InitializeComponent();
        }

        private void CalibrateBody_OnLoaded(object sender, RoutedEventArgs e)
        {
            _sensor = KinectSensor.GetDefault();
            if (_sensor != null)
            {
                //Open the sensor
                _sensor.Open();

                //initialize bodies
                bodies = new Body[6];
                //set onebody
                _oneBody = 0;

                //set width and height of bodyCanvas
                canvasWidth = bodyCanvas.ActualWidth;
                canvasHeight = bodyCanvas.ActualHeight;

                //initialize the colorCamera
                colorCamera = new ColorCamera(_sensor.ColorFrameSource.FrameDescription.Width,
                                              _sensor.ColorFrameSource.FrameDescription.Height);

                //Set boneCalibration to 0
                _calibrated = false;

                //initialize the joints
                jointsDictionary = new Dictionary<JointType, MyJoint>();

                //centre of bodyJoints
                jointsDictionary.Add(JointType.Head, Head.Instance);
                jointsDictionary.Add(JointType.Neck, Neck.Instance);
                jointsDictionary.Add(JointType.SpineShoulder, SpineShoulder.Instance);
                jointsDictionary.Add(JointType.SpineMid, SpineMid.Instance);
                jointsDictionary.Add(JointType.SpineBase, SpineBase.Instance);

                //Toppart of bodyJoints
                //right
                jointsDictionary.Add(JointType.ShoulderRight, RightShoulder.Instance);
                jointsDictionary.Add(JointType.ElbowRight, RightElbow.Instance);
                jointsDictionary.Add(JointType.HandRight, RightHand.Instance);
                //left
                jointsDictionary.Add(JointType.ShoulderLeft, LeftShoulder.Instance);
                jointsDictionary.Add(JointType.ElbowLeft, LeftElbow.Instance);
                jointsDictionary.Add(JointType.HandLeft, LeftHand.Instance);

                //Bottompart of bodyJoints
                //right
                jointsDictionary.Add(JointType.HipRight, RightHip.Instance);
                jointsDictionary.Add(JointType.KneeRight, RightKnee.Instance);
                jointsDictionary.Add(JointType.AnkleRight, RightAnkle.Instance);
                jointsDictionary.Add(JointType.FootRight, RightFoot.Instance);
                //left
                jointsDictionary.Add(JointType.HipLeft, LeftHip.Instance);
                jointsDictionary.Add(JointType.KneeLeft, LeftKnee.Instance);
                jointsDictionary.Add(JointType.AnkleLeft, LeftAnkle.Instance);
                jointsDictionary.Add(JointType.FootLeft, LeftFoot.Instance);

                //initialize the bones
                bonesDictionary = new List<MyBone>();

                //Centre of bones
                bonesDictionary.Add(HeadNeck.Instance);
                bonesDictionary.Add(NeckSpineShoulder.Instance);
                bonesDictionary.Add(SpineShoulderMid.Instance);
                bonesDictionary.Add(SpineMidBase.Instance);

                //Toppart of bones
                //right
                bonesDictionary.Add(RightHandElbow.Instance);
                bonesDictionary.Add(RightElbowShoulder.Instance);
                bonesDictionary.Add(RightShoulderSpine.Instance);
                //left
                bonesDictionary.Add(LeftHandElbow.Instance);
                bonesDictionary.Add(LeftElbowShoulder.Instance);
                bonesDictionary.Add(LeftShoulderSpine.Instance);

                //Bottompart of bones
                //right
                bonesDictionary.Add(RightHipSpine.Instance);
                bonesDictionary.Add(RightHipKnee.Instance);
                bonesDictionary.Add(RightKneeAnkle.Instance);
                bonesDictionary.Add(RightAnkleFoot.Instance);
                //left
                bonesDictionary.Add(LeftHipSpine.Instance);
                bonesDictionary.Add(LeftHipKnee.Instance);
                bonesDictionary.Add(LeftKneeAnkle.Instance);
                bonesDictionary.Add(LeftAnkleFoot.Instance);

                //Open the multiframereader
                _frameReader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Body);
                _frameReader.MultiSourceFrameArrived += _frameReader_MultiSourceFrameArrived;
                ;
            }
        }

        private void CalibrateBody_OnClosing(object sender, CancelEventArgs e)
        {
            if (_frameReader != null)
            {
                _frameReader.Dispose();
                _frameReader = null;
            }
        }

        private void _frameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            //aquire the frame
            var frame = e.FrameReference.AcquireFrame();

            //Start the colorFrameReader
            using (ColorFrame colorFrame = frame.ColorFrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    camera.Source = colorCamera.ByteStringToSource(colorFrame);
                }
            }

            //Start the bodyFrameReader
            using (BodyFrame bodyFrame = frame.BodyFrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    bodyFrame.GetAndRefreshBodyData(bodies);
                    bodyCanvas.Children.Clear();
                    DataRecieved(bodyFrame);
                }
            }
        }
        public void DataRecieved(BodyFrame bodyFrame)
        {
            int trackingIdStillExists = 0;
            foreach (Body body in bodies)
            {
                //check for bodies
                if (body.IsTracked)
                {

                    if (_oneBody == 0)
                    {
                        _oneBody = body.TrackingId;
                    }
                    if (_oneBody == body.TrackingId)
                    {
                        foreach (JointType jointType in jointsDictionary.Keys)
                        {
                            // sometimes the depth(Z) of an inferred joint may show as negative
                            // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                            CameraSpacePoint position = body.Joints[jointType].Position;

                            if (position.Z < 0)
                            {
                                position.Z = 0.1f;
                            }

                            //change the jointValues so that they align with the colorCamera
                            ColorSpacePoint csPoint = _sensor.CoordinateMapper.MapCameraPointToColorSpace(position);

                            //update position of Joints
                            jointsDictionary[jointType].Update(new Point(csPoint.X, csPoint.Y));

                        }
                        //draw the body and save all joints every frame
                        DrawBody(body.Joints);

                        //save the bones every frame
                        DrawBones();

                        //If the person logs in the first time there will be a calibration
                        CalibrateWhenPossible();

                    }
                }
                if (body.TrackingId != _oneBody)
                {
                    trackingIdStillExists++;
                    if (trackingIdStillExists >= bodies.Length)
                    {
                        _oneBody = 0;
                    }
                }
            }

        }

        public void DrawBody(IReadOnlyDictionary<JointType, Joint> joints)
        {
            foreach (JointType jointType in jointsDictionary.Keys)
            {
                if (joints[jointType].TrackingState == TrackingState.Tracked)
                {
                    double diffRight = RightElbowShoulder.Instance.GetDistance().Y + RightHandElbow.Instance.GetDistance().Y;
                    double diffLeft = LeftElbowShoulder.Instance.GetDistance().Y + LeftHandElbow.Instance.GetDistance().Y;
                    //set the correct positions
                    double positionX = (jointsDictionary[jointType].GetPosition().X * canvasWidth /
                                        colorCamera.ScreenWidth) - 5;
                    double positionY = (jointsDictionary[jointType].GetPosition().Y * canvasHeight /
                                        colorCamera.ScreenHeight) - 5;
                    Ellipse circle = new Ellipse()
                    {
                        Width = 20,
                        Height = 20,
                        Fill = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0))
                    };
                    if (diffRight < 40 && (jointType == JointType.HandRight || jointType == JointType.ElbowRight || jointType == JointType.ShoulderRight))
                    {
                        
                        circle = new Ellipse()
                        {
                            Width = 20,
                            Height = 20,
                            Fill = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0))
                        };
                    }
                    if (diffLeft < 40 && (jointType == JointType.HandLeft || jointType == JointType.ElbowLeft || jointType == JointType.ShoulderLeft))
                    {

                        circle = new Ellipse()
                        {
                            Width = 20,
                            Height = 20,
                            Fill = new SolidColorBrush(Color.FromArgb(255, 0, 255, 0))
                        };
                    }

                    bodyCanvas.Children.Add(circle);
                    Canvas.SetLeft(circle, positionX);
                    Canvas.SetTop(circle, positionY);
                    
                }

            }
        }

        public void DrawBones()
        {
            foreach (var bone in bonesDictionary)
            {
                bone.Update();
            }
        }

        public void CalibrateWhenPossible()
        {
            double diffRight = RightElbowShoulder.Instance.GetDistance().Y + RightHandElbow.Instance.GetDistance().Y;
            double diffLeft = LeftElbowShoulder.Instance.GetDistance().Y + LeftHandElbow.Instance.GetDistance().Y;

            if ((diffLeft > 40 || diffRight > 40) && _calibrated == false)
            {
                _timer = 0;
                title.Text = "Spread your arms into a horizontal line";
            }
            else
            {
                //Give the amount of seconds before the calibration
                if (_timer < 89 && _calibrated == false)
                {
                    if (_timer % 30 == 0)
                    {
                        title.Text = "hold stil in " + (3 - (_timer / 30)).ToString() + "";
                    }
                }

                //The calibration happens in 3 frames
                if (_timer >= 90 && _timer < 93 && _calibrated == false)
                {
                    //calibrate the distance between items
                    CalibrateDistanceBetweenBones();
                    title.Text = "You are being saved";
                }
                if (_timer >= 110 && _calibrated == false)
                {
                    _calibrated = true;
                    title.Text = "Your body is calibrated and we send you back to the menu";
                }

                if (_timer < 150)
                {
                    _timer++;
                }

                if (_timer == 150)
                {
                    //sent you back to menu
                    Menu menu = new Menu();
                    menu.Show();
                    this.Close();
                }

            }
        }

        public void CalibrateDistanceBetweenBones()
        {
            foreach (var bone in bonesDictionary)
            {
                bone.SaveBasePosition();
            }
        }
    }
}
