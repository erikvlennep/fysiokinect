﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightHandElbow : MyBone
    {
        private static RightHandElbow _instance;
        public static RightHandElbow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightHandElbow();
                }
                return _instance;
            }
        }

        private RightHandElbow()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightElbow.Instance.GetPosition();
            position2 = RightHand.Instance.GetPosition();
        }
    }
}
