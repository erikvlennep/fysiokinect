﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftHipKnee : MyBone
    {
        private static LeftHipKnee _instance;

        public static LeftHipKnee Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftHipKnee();
                }
                return _instance;
            }
        }

        private LeftHipKnee()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = LeftHip.Instance.GetPosition();
            position2 = LeftKnee.Instance.GetPosition();
        }
    }
}
