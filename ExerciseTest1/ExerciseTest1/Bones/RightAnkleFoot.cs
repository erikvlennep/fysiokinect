﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightAnkleFoot : MyBone
    {
        private static RightAnkleFoot _instance;

        public static RightAnkleFoot Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightAnkleFoot();
                }
                return _instance;
            }
        }

        private RightAnkleFoot()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightAnkle.Instance.GetPosition();
            position2 = RightFoot.Instance.GetPosition();
        }
    }
}
