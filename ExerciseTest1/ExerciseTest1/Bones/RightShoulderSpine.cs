﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightShoulderSpine : MyBone
    {
        private static RightShoulderSpine _instance;

        public static RightShoulderSpine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightShoulderSpine();
                }
                return _instance;
            }
        }

        private RightShoulderSpine()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightShoulder.Instance.GetPosition();
            position2 = SpineShoulder.Instance.GetPosition();
        }
    }
}
