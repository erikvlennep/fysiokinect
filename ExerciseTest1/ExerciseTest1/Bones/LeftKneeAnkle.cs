﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftKneeAnkle : MyBone
    {
        private static LeftKneeAnkle _instance;

        public static LeftKneeAnkle Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftKneeAnkle();
                }
                return _instance;
            }
        }

        private LeftKneeAnkle()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = LeftKnee.Instance.GetPosition();
            position2 = LeftAnkle.Instance.GetPosition();
        }
    }
}
