﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftElbowShoulder : MyBone
    {
        private static LeftElbowShoulder _instance;

        public static LeftElbowShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftElbowShoulder();
                }
                return _instance;
            }
        }

        private LeftElbowShoulder()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = LeftElbow.Instance.GetPosition();
            position2 = LeftShoulder.Instance.GetPosition();
        }
    }
}
