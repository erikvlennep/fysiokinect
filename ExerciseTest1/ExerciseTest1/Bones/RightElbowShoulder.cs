﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightElbowShoulder : MyBone
    {
        private static RightElbowShoulder _instance;

        public static RightElbowShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightElbowShoulder();
                }
                return _instance;
            }
        }

        private RightElbowShoulder()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightElbow.Instance.GetPosition();
            position2 = RightShoulder.Instance.GetPosition();
        }
    }
}
