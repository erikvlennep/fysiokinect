﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightHipSpine : MyBone
    {
        private static RightHipSpine _instance;

        public static RightHipSpine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightHipSpine();
                }
                return _instance;
            }
        }

        private RightHipSpine()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = SpineBase.Instance.GetPosition();
            position2 = RightHip.Instance.GetPosition();
        }
    }
}
