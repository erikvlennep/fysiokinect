﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class SpineShoulderMid : MyBone
    {
        private static SpineShoulderMid _instance;

        public static SpineShoulderMid Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpineShoulderMid();
                }
                return _instance;
            }
        }

        private SpineShoulderMid()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = SpineShoulder.Instance.GetPosition();
            position2 = SpineMid.Instance.GetPosition();
        }
    }
}
