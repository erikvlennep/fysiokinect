﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftShoulderSpine : MyBone
    {
        private static LeftShoulderSpine _instance;

        public static LeftShoulderSpine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftShoulderSpine();
                }
                return _instance;
            }
        }

        private LeftShoulderSpine()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightShoulder.Instance.GetPosition();
            position2 = SpineShoulder.Instance.GetPosition();
        }
    }
}
