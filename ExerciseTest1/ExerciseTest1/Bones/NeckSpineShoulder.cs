﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class NeckSpineShoulder : MyBone
    {
        private static NeckSpineShoulder _instance;

        public static NeckSpineShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new NeckSpineShoulder();
                }
                return _instance;
            }
        }

        private NeckSpineShoulder()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = Neck.Instance.GetPosition();
            position2 = SpineShoulder.Instance.GetPosition();
        }
    }
}
