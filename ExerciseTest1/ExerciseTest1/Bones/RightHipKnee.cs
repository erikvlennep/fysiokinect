﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightHipKnee : MyBone
    {
        private static RightHipKnee _instance;

        public static RightHipKnee Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightHipKnee();
                }
                return _instance;
            }
        }

        private RightHipKnee()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightHip.Instance.GetPosition();
            position2 = RightKnee.Instance.GetPosition();
        }
    }
}
