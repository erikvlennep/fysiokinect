﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class HeadNeck : MyBone
    {
        private static HeadNeck _instance;

        public static HeadNeck Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new HeadNeck();
                }
                return _instance;
            }
        }

        private HeadNeck()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = Head.Instance.GetPosition();
            position2 = Neck.Instance.GetPosition();
        }
    }
}
