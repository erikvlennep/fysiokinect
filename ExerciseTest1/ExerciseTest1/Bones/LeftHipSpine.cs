﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftHipSpine : MyBone
    {
        private static LeftHipSpine _instance;

        public static LeftHipSpine Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftHipSpine();
                }
                return _instance;
            }
        }

        private LeftHipSpine()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = SpineBase.Instance.GetPosition();
            position2 = LeftHip.Instance.GetPosition();
        }
    }
}
