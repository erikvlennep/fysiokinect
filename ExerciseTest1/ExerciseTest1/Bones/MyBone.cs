﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Bones
{
    public abstract class MyBone
    {
        protected List<Point> distance;

        protected Point calibratedDistance;

        protected Point position1;
        protected Point position2;

        protected bool calibratedDistanceIsSet;

        protected abstract void GetPositions();

        public virtual void Update()
        {
            GetPositions();
            distance.Add(CalculateDifference(position1, position2));
        }

        public Point GetDistance()
        {
            return distance.LastOrDefault();
        }

        public virtual void SaveBasePosition()
        {
            GetPositions();
            calibratedDistanceIsSet = true;
            calibratedDistance = CalculateDifference(position1, position2);
        }

        public Point GetCalibratedDistance()
        {
            return calibratedDistance;
        }

        public Point GetDifferenceFromBase()
        {
            return CalculateDifference(calibratedDistance, distance.LastOrDefault());
        }

        protected Point CalculateDifference(Point pos1, Point pos2)
        {
            double midX = pos1.X - pos2.X;
            double midY = pos1.Y - pos2.Y;

            if (pos2.X > pos1.X)
            {
                midX = pos2.X - pos1.X;
            }
            if (pos2.Y > pos1.Y)
            {
                midY = pos2.Y - pos1.Y;
            }

            return new Point(midX, midY);
        }

        public bool BasePositionSet()
        {
            return calibratedDistanceIsSet;

        }
    }
}
