﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftAnkleFoot : MyBone
    {
        private static LeftAnkleFoot _instance;

        public static LeftAnkleFoot Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftAnkleFoot();
                }
                return _instance;
            }
        }

        private LeftAnkleFoot()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = LeftAnkle.Instance.GetPosition();
            position2 = LeftFoot.Instance.GetPosition();
        }
    }
}
