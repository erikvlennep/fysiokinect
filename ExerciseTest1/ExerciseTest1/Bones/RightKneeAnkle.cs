﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class RightKneeAnkle : MyBone
    {
        private static RightKneeAnkle _instance;

        public static RightKneeAnkle Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightKneeAnkle();
                }
                return _instance;
            }
        }

        private RightKneeAnkle()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightKnee.Instance.GetPosition();
            position2 = RightAnkle.Instance.GetPosition();
        }
    }
}
