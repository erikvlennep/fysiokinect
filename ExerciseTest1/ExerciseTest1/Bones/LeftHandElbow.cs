﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class LeftHandElbow : MyBone
    {
        private static LeftHandElbow _instance;

        public static LeftHandElbow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftHandElbow();
                }
                return _instance;
            }
        }

        private LeftHandElbow()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = RightHand.Instance.GetPosition();
            position2 = RightElbow.Instance.GetPosition();
        }
    }
}
