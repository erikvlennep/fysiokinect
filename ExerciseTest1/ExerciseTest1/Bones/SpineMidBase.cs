﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ExerciseTest1.Joints;

namespace ExerciseTest1.Bones
{
    class SpineMidBase : MyBone
    {
        private static SpineMidBase _instance;

        public static SpineMidBase Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpineMidBase();
                }
                return _instance;
            }
        }

        private SpineMidBase()
        {
            distance = new List<Point>();
        }

        protected override void GetPositions()
        {
            position1 = SpineMid.Instance.GetPosition();
            position2 = SpineBase.Instance.GetPosition();
        }
    }
}
