﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ExerciseTest1.Pages;
using Microsoft.Kinect;
using Microsoft.Kinect.Wpf.Controls;

namespace ExerciseTest1
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();

            KinectRegion.SetKinectRegion(this, RegionKinect);

            RegionKinect.KinectSensor = KinectSensor.GetDefault();
        }

        private void FirstExercise(object sender, RoutedEventArgs e)
        {
            Window firstExercise = new MainWindow();
            firstExercise.Show();
            this.Close();
        }

        private void CalibrateBody(object sender, RoutedEventArgs e)
        {
            Window calibrateBody = new CalibrateBody();
            calibrateBody.Show();
            this.Close();
        }
    }
}
