﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Microsoft.Kinect;

namespace ExerciseTest1.Camera
{
    public class ColorCamera
    {
        public int ScreenWidth { get; set; }
        public int ScreenHeight { get; set; }

        private PixelFormat format;
        private byte[] pixels;
        private int stride;

        /// <summary>
        /// Set the width and height for the colorCamera
        /// </summary>
        /// <param name="width">Width resolution of colorCamera</param>
        /// <param name="height">Height resolution of colorCamera</param>
        public ColorCamera(int width, int height)
        {
            //kinectColorFrame height and width
            ScreenWidth = width;
            ScreenHeight = height;

            //Colorformat
            format = PixelFormats.Bgr32;

            //pixelByteStream for RGBA
            pixels = new byte[ScreenWidth * ScreenHeight * ((format.BitsPerPixel) / 8)];

            //Stride for colorScreen
            stride = ScreenWidth * format.BitsPerPixel / 8;
        }

        public ImageSource ByteStringToSource(ColorFrame frame)
        {
            if (frame.RawColorImageFormat == ColorImageFormat.Bgra)
            {
                frame.CopyRawFrameDataToArray(pixels);
            }
            else
            {
                frame.CopyConvertedFrameDataToArray(pixels, ColorImageFormat.Bgra);
            }

            return BitmapSource.Create(ScreenWidth, ScreenHeight, 96, 96, format, null, pixels, stride);
        }
    }
}
