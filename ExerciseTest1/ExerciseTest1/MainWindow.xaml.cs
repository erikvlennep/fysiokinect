﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ExerciseTest1.Bones;
using ExerciseTest1.Camera;
using ExerciseTest1.Joints;
using ExerciseTest1.Pages;
using Microsoft.Kinect;
using Microsoft.Kinect.Wpf.Controls;

namespace ExerciseTest1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Needed for activating and deactivating the KinectSensor
        /// </summary>
        private KinectSensor _sensor;

        /// <summary>
        /// Multiple frames are needed
        /// </summary>
        private MultiSourceFrameReader _frameReader;

        /// <summary>
        /// Check all the bodies in the room. max: 6
        /// </summary>
        private Body[] bodies;

        /// <summary>
        /// The colorcamera of the kinect. 
        /// </summary>
        private ColorCamera colorCamera;

        /// <summary>
        /// The width of the canvas
        /// </summary>
        public double canvasWidth;

        /// <summary>
        /// The height of the canvas
        /// </summary>
        public double canvasHeight;

        /// <summary>
        /// Connection between jointtypes and joints
        /// </summary>
        private Dictionary<JointType, MyJoint> jointsDictionary;

        /// <summary>
        /// The bones for the connection between joints 
        /// </summary>
        private List<MyBone> bonesDictionary;

        /// <summary>
        /// Timer for calibration
        /// </summary>
        private int _timer;

        /// <summary>
        /// Keep track of the first person to join the room
        /// </summary>
        private ulong _oneBody;

        public MainWindow()
        {
            InitializeComponent();
            KinectRegion.SetKinectRegion(this, RegionKinect);
            RegionKinect.KinectSensor = KinectSensor.GetDefault();
            if (HeadNeck.Instance.BasePositionSet())
            {
                Loaded += MainWindow_Loaded;
            }
            else
            {
                Loaded += MainWindow_Loaded1;
            }
        }

        private void MainWindow_Loaded1(object sender, RoutedEventArgs e)
        {

            calibrateBodyButton.Visibility = Visibility.Visible;

        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _sensor = KinectSensor.GetDefault();

            if (_sensor != null)
            {
                //Open the sensor
                _sensor.Open();

                //initialize bodies
                bodies = new Body[6];

                //set onebody
                _oneBody = 0;

                //set width and height of bodyCanvas
                canvasWidth = bodyCanvas.ActualWidth;
                canvasHeight = bodyCanvas.ActualHeight;

                //initialize the colorCamera
                colorCamera = new ColorCamera(_sensor.ColorFrameSource.FrameDescription.Width, _sensor.ColorFrameSource.FrameDescription.Height);

                //initialize the joints
                jointsDictionary = new Dictionary<JointType, MyJoint>();

                //centre of bodyJoints
                jointsDictionary.Add(JointType.Head, Head.Instance);
                jointsDictionary.Add(JointType.Neck, Neck.Instance);
                jointsDictionary.Add(JointType.SpineShoulder, SpineShoulder.Instance);
                jointsDictionary.Add(JointType.SpineMid, SpineMid.Instance);
                jointsDictionary.Add(JointType.SpineBase, SpineBase.Instance);

                //Toppart of bodyJoints
                //right
                jointsDictionary.Add(JointType.ShoulderRight, RightShoulder.Instance);
                jointsDictionary.Add(JointType.ElbowRight, RightElbow.Instance);
                jointsDictionary.Add(JointType.HandRight, RightHand.Instance);
                //left
                jointsDictionary.Add(JointType.ShoulderLeft, LeftShoulder.Instance);
                jointsDictionary.Add(JointType.ElbowLeft, LeftElbow.Instance);
                jointsDictionary.Add(JointType.HandLeft, LeftHand.Instance);

                //Bottompart of bodyJoints
                //right
                jointsDictionary.Add(JointType.HipRight, RightHip.Instance);
                jointsDictionary.Add(JointType.KneeRight, RightKnee.Instance);
                jointsDictionary.Add(JointType.AnkleRight, RightAnkle.Instance);
                jointsDictionary.Add(JointType.FootRight, RightFoot.Instance);
                //left
                jointsDictionary.Add(JointType.HipLeft, LeftHip.Instance);
                jointsDictionary.Add(JointType.KneeLeft, LeftKnee.Instance);
                jointsDictionary.Add(JointType.AnkleLeft, LeftAnkle.Instance);
                jointsDictionary.Add(JointType.FootLeft, LeftFoot.Instance);

                //initialize the bones
                bonesDictionary = new List<MyBone>();

                //Centre of bones
                bonesDictionary.Add(HeadNeck.Instance);
                bonesDictionary.Add(NeckSpineShoulder.Instance);
                bonesDictionary.Add(SpineShoulderMid.Instance);
                bonesDictionary.Add(SpineMidBase.Instance);

                //Toppart of bones
                //right
                bonesDictionary.Add(RightHandElbow.Instance);
                bonesDictionary.Add(RightElbowShoulder.Instance);
                bonesDictionary.Add(RightShoulderSpine.Instance);
                //left
                bonesDictionary.Add(LeftHandElbow.Instance);
                bonesDictionary.Add(LeftElbowShoulder.Instance);
                bonesDictionary.Add(LeftShoulderSpine.Instance);

                //Bottompart of bones
                //right
                bonesDictionary.Add(RightHipSpine.Instance);
                bonesDictionary.Add(RightHipKnee.Instance);
                bonesDictionary.Add(RightKneeAnkle.Instance);
                bonesDictionary.Add(RightAnkleFoot.Instance);
                //left
                bonesDictionary.Add(LeftHipSpine.Instance);
                bonesDictionary.Add(LeftHipKnee.Instance);
                bonesDictionary.Add(LeftKneeAnkle.Instance);
                bonesDictionary.Add(LeftAnkleFoot.Instance);

                //Open the multiframereader
                _frameReader = _sensor.OpenMultiSourceFrameReader(FrameSourceTypes.Color | FrameSourceTypes.Body);
                _frameReader.MultiSourceFrameArrived += _frameReader_MultiSourceFrameArrived;

            }
        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (_frameReader != null)
            {
                _frameReader.Dispose();
                _frameReader = null;
            }
        }

        private void _frameReader_MultiSourceFrameArrived(object sender, MultiSourceFrameArrivedEventArgs e)
        {
            //aquire the frame
            var frame = e.FrameReference.AcquireFrame();

            //Start the colorFrameReader
            using (ColorFrame colorFrame = frame.ColorFrameReference.AcquireFrame())
            {
                if (colorFrame != null)
                {
                    camera.Source = colorCamera.ByteStringToSource(colorFrame);
                }
            }

            //Start the bodyFrameReader
            using (BodyFrame bodyFrame = frame.BodyFrameReference.AcquireFrame())
            {
                if (bodyFrame != null)
                {
                    bodyFrame.GetAndRefreshBodyData(bodies);
                    bodyCanvas.Children.Clear();
                    DataRecieved(bodyFrame);
                }
            }
        }



        public void DataRecieved(BodyFrame bodyFrame)
        {
            int trackingIdStillExists = 0;
            foreach (Body body in bodies)
            {
                //check for bodies
                if (body.IsTracked)
                {

                    if (_oneBody == 0)
                    {
                        _oneBody = body.TrackingId;
                    }
                    if (_oneBody == body.TrackingId)
                    {
                        foreach (JointType jointType in jointsDictionary.Keys)
                        {
                            // sometimes the depth(Z) of an inferred joint may show as negative
                            // clamp down to 0.1f to prevent coordinatemapper from returning (-Infinity, -Infinity)
                            CameraSpacePoint position = body.Joints[jointType].Position;

                            if (position.Z < 0)
                            {
                                position.Z = 0.1f;
                            }

                            //change the jointValues so that they align with the colorCamera
                            ColorSpacePoint csPoint = _sensor.CoordinateMapper.MapCameraPointToColorSpace(position);

                            //update position of Joints
                            jointsDictionary[jointType].Update(new Point(csPoint.X, csPoint.Y));

                        }
                        //draw the body and save all joints every frame
                        DrawBody(body.Joints);

                        //save the bones every frame
                        DrawBones();

                        StartFirstExercise();
                    }
                }
                if (body.TrackingId != _oneBody)
                {
                    trackingIdStillExists++;
                    if (trackingIdStillExists >= bodies.Length)
                    {
                        _oneBody = 0;
                    }
                }
            }
        }

        public void DrawBody(IReadOnlyDictionary<JointType, Joint> joints)
        {
            foreach (JointType jointType in jointsDictionary.Keys)
            {
                if (joints[jointType].TrackingState == TrackingState.Tracked)
                {
                    //set the correct positions
                    double positionX = (jointsDictionary[jointType].GetPosition().X * canvasWidth /
                                        colorCamera.ScreenWidth) - 5;
                    double positionY = (jointsDictionary[jointType].GetPosition().Y * canvasHeight /
                                        colorCamera.ScreenHeight) - 5;
                    Ellipse circle = new Ellipse()
                    {
                        Width = 20,
                        Height = 20,
                        Fill = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0))
                    };
                    bodyCanvas.Children.Add(circle);
                    Canvas.SetLeft(circle, positionX);
                    Canvas.SetTop(circle, positionY);
                }
            }
        }

        public void DrawBones()
        {
            foreach (var bone in bonesDictionary)
            {
                bone.Update();
            }
        }

        private bool _armInBasePosition = false;
        private bool _handToShoulder = false;
        private bool _handFromShoulder = false;

        //the first exercise: Move your hand to your shoulder and back
        public void StartFirstExercise()
        {

            double diffX = RightHandElbow.Instance.GetDifferenceFromBase().X +
                    RightElbowShoulder.Instance.GetDifferenceFromBase().X;
            double diffHandShoulderY = RightHand.Instance.GetPosition().Y - RightShoulder.Instance.GetPosition().Y;

            double calibratedDist = RightHandElbow.Instance.GetCalibratedDistance().X +
                RightElbowShoulder.Instance.GetCalibratedDistance().X;

            if (_handToShoulder == false && _handFromShoulder == false && _armInBasePosition == false)
            {
                title.Text = "Stretch your right arm to start exercise one";
                if (diffX < 20)
                {
                    title.Text = "Exercise 1: Move your right hand towards your shoulder";
                    _armInBasePosition = true;
                }
            }

            if (_handToShoulder == false && _armInBasePosition == true)
            {
                if (diffHandShoulderY > 60 || diffHandShoulderY < -60)
                {
                    subtitle.Text = "You are at" + (diffX / calibratedDist * 100).ToString("N1") + "% of the max angle,\r\n but your right hand should be at the same hight as your sholder.";
                }
                else
                {
                    subtitle.Text = "You are at" + (diffX / calibratedDist * 100).ToString("N1") + "% of the max angle";
                    if (diffX > calibratedDist*0.5)
                    {
                        subtitle.Text = "You are at 75% of the max angle";
                        title.Text = "Stretch your hand to its original position!";
                        _handToShoulder = true;
                    }
                }

            }
            if (_handToShoulder == true && _handFromShoulder == false)
            {
                if (diffHandShoulderY > 60 || diffHandShoulderY < -60)
                {
                    subtitle.Text = "You are at" + (diffX / calibratedDist * 100).ToString("N1") + "% of the max angle, but your right hand should be at the same hight as your sholder.";
                }
                else
                {
                    subtitle.Text = "You are at" + (diffX / calibratedDist * 100).ToString("N1") + "% of the max angle";
                    if (diffX < 20)
                    {
                        subtitle.Text = "Your arm is back at the base position";
                        title.Text = "You did it!";
                        _handFromShoulder = true;
                    }
                }
            }
        }

        private void CalibrateBodyButton_OnClick(object sender, RoutedEventArgs e)
        {
            Window calibrateBody = new CalibrateBody();
            calibrateBody.Show();
            this.Close();
        }

        private void BackButton_OnClick(object sender, RoutedEventArgs e)
        {
            Window menu = new Menu();
            menu.Show();
            this.Close();
        }
    }
}
