﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class SpineMid : MyJoint
    {
        private static SpineMid _instance;

        public static SpineMid Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpineMid();
                }

                return _instance;
            }
        }

        private SpineMid()
        {
            positions = new List<Point>();
        }
    }
}
