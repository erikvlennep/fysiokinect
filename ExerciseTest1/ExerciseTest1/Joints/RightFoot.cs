﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class RightFoot : MyJoint
    {
        private static RightFoot _instance;

        public static RightFoot Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightFoot();
                }

                return _instance;
            }
        }

        private RightFoot()
        {
            positions = new List<Point>();
        }
    }
}
