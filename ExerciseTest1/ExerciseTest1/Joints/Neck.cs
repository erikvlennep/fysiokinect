﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class Neck : MyJoint
    {
        private static Neck _instance;

        public static Neck Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Neck();
                }

                return _instance;
            }
        }

        private Neck()
        {
            positions = new List<Point>();
        }
    }
}
