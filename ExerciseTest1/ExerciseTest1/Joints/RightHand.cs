﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    public class RightHand : MyJoint
    {
        private static RightHand _instance;

        public static RightHand Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightHand();
                }

                return _instance;
            }
        }
        
        private RightHand()
        {
            positions = new List<Point>();
        }
    }
}
