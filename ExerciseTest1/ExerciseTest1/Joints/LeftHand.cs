﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftHand : MyJoint
    {
        private static LeftHand _instance;

        public static LeftHand Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftHand();
                }

                return _instance;
            }
        }

        private LeftHand()
        {
            positions = new List<Point>();
        }
    }
}
