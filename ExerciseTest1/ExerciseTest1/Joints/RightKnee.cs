﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class RightKnee : MyJoint
    {
        private static RightKnee _instance;

        public static RightKnee Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightKnee();
                }

                return _instance;
            }
        }

        private RightKnee()
        {
            positions = new List<Point>();
        }
    }
}
