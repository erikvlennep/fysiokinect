﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftShoulder : MyJoint
    {
        private static LeftShoulder _instance;

        public static LeftShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftShoulder();
                }

                return _instance;
            }
        }

        private LeftShoulder()
        {
            positions = new List<Point>();
        }
    }
}
