﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class SpineBase : MyJoint
    {
        private static SpineBase _instance;

        public static SpineBase Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpineBase();
                }

                return _instance;
            }
        }

        private SpineBase()
        {
            positions = new List<Point>();
        }
    }
}
