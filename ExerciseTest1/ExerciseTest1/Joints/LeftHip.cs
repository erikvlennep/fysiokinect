﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftHip : MyJoint
    {
        private static LeftHip _instance;

        public static LeftHip Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftHip();
                }

                return _instance;
            }
        }

        private LeftHip()
        {
            positions = new List<Point>();
        }
    }
}
