﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class RightAnkle : MyJoint
    {
        private static RightAnkle _instance;

        public static RightAnkle Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightAnkle();
                }

                return _instance;
            }
        }

        private RightAnkle()
        {
            positions = new List<Point>();
        }
    }
}
