﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftFoot : MyJoint
    {
        private static LeftFoot _instance;

        public static LeftFoot Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftFoot();
                }

                return _instance;
            }
        }

        private LeftFoot()
        {
            positions = new List<Point>();
        }
    }
}
