﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class RightHip : MyJoint
    {
        private static RightHip _instance;

        public static RightHip Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightHip();
                }

                return _instance;
            }
        }

        private RightHip()
        {
            positions = new List<Point>();
        }
    }
}
