﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    public abstract class MyJoint
    {
        protected List<Point> positions;
       
        public void Update(Point position)
        {
            positions.Add(position);
        }

        public Point GetPosition()
        {
            return positions.LastOrDefault();
        }

        /// <summary>
        /// Get the Highest position
        /// </summary>
        /// <param name="seconds">amount of seconds needed</param>
        /// <returns>position.x</returns>
        public double HighestPosition(int seconds)
        {
            double maxValue = 0;

            if (positions.Count > 30)
            {
                var LstItems = positions.Skip(positions.Count() - 30);
                foreach (Point position in LstItems)
                {
                    if (position.X > maxValue)
                    {
                        maxValue = position.X;
                    }
                }
            }

            return maxValue;
        }
    }
}
