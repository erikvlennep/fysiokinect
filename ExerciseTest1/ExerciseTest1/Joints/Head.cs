﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class Head : MyJoint
    {
        private static Head _instance;

        public static Head Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Head();
                }

                return _instance;
            }
        }

        private Head()
        {
            positions = new List<Point>();
        }
    }
}
