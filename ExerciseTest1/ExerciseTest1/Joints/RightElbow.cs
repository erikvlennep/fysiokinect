﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class RightElbow : MyJoint
    {
        private static RightElbow _instance;

        public static RightElbow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightElbow();
                }

                return _instance;
            }
        }

        private RightElbow()
        {
            positions = new List<Point>();
        }
    }
}
