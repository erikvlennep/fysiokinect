﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftAnkle : MyJoint
    {
        private static LeftAnkle _instance;

        public static LeftAnkle Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftAnkle();
                }

                return _instance;
            }
        }

        private LeftAnkle()
        {
            positions = new List<Point>();
        }
    }
}
