﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftElbow : MyJoint
    {
        private static LeftElbow _instance;

        public static LeftElbow Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftElbow();
                }

                return _instance;
            }
        }

        private LeftElbow()
        {
            positions = new List<Point>();
        }
    }
}
