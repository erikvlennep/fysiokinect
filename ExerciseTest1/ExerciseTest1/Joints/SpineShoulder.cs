﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class SpineShoulder : MyJoint
    {
        private static SpineShoulder _instance;

        public static SpineShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new SpineShoulder();
                }

                return _instance;
            }
        }

        private SpineShoulder()
        {
            positions = new List<Point>();
        }
    }
}
