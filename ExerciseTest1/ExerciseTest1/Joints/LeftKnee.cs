﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    class LeftKnee : MyJoint
    {
        private static LeftKnee _instance;

        public static LeftKnee Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new LeftKnee();
                }

                return _instance;
            }
        }

        private LeftKnee()
        {
            positions = new List<Point>();
        }
    }
}
