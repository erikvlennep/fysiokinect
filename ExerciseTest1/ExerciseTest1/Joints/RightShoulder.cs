﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ExerciseTest1.Joints
{
    public class RightShoulder : MyJoint
    {
        private static RightShoulder _instance;
        
        public static RightShoulder Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new RightShoulder();
                }

                return _instance;
            }
        }

        private RightShoulder()
        {
            positions = new List<Point>();
        }
    }
}
